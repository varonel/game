#include "MoveCommand.h"

namespace command {
    void MoveCommand::execute() {
        character.move(x, y);
    }

    MoveCommand::MoveCommand(model::Character &character, int x, int y) : character(character), x(x), y(y) {}
} // namespace command
