#define EVENT_TYPE_TICK 1
#define EVENT_TYPE_TURN 2
#define EVENT_TYPE_CMD  3

#ifndef ENGINE_EVENT_H
#define ENGINE_EVENT_H

#include "../command/BaseCommand.h"
#include <string>
#include <utility>
#include <vector>

namespace event {
    namespace _internal {
        using namespace std;

        struct EventType {
            const int id;
            string cmd;
            vector<string> context;

            explicit EventType(int id) : id(id) {}

            EventType(const int id, string&& cmd, vector<string>&& context)
                    : id(id), cmd(move(cmd)), context(move(context)) {}
        };
    }
    using _internal::EventType;
}

#endif //ENGINE_EVENT_H
