#ifndef GAME_TURNEVENTSUBSCRIBER_H
#define GAME_TURNEVENTSUBSCRIBER_H

#include "../../engine/src/event/EventSubscriber.h"

namespace event {
    class TurnEventSubscriber : public EventSubscriber {
    public:
        void run(const EventType &e) override;
    };
} // namespace event

#endif //GAME_TURNEVENTSUBSCRIBER_H
