#ifndef GAME_MOVECOMMAND_H
#define GAME_MOVECOMMAND_H

#include "../../engine/src/command/BaseCommand.h"
#include "../model/Character.h"

namespace command {
    class MoveCommand : public command::BaseCommand {
    public:
        MoveCommand(model::Character &character, int x, int y);

        void execute() override;

    private:
        model::Character &character;
        int x;
        int y;
    };
}  // namespace command

#endif //GAME_MOVECOMMAND_H
