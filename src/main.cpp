#include "../engine/src/Engine.h"

#include "event/CommandEventSubscriber.h"
#include "event/TickEventSubscriber.h"
#include "event/TurnEventSubscriber.h"
#include "model/Character.h"
#include "model/CharacterList.h"

// *,-cppcoreguidelines-*,-android-*,-bugprone-bool-pointer-implicit-conversion,-bugprone-exception-escape,-cert-env33-c,-cert-dcl50-cpp,-cert-dcl59-cpp,-cppcoreguidelines-avoid-goto,-cppcoreguidelines-avoid-magic-numbers,-cppcoreguidelines-no-malloc,-cppcoreguidelines-owning-memory,-cppcoreguidelines-pro-bounds-array-to-pointer-decay,-cppcoreguidelines-pro-bounds-constant-array-index,-cppcoreguidelines-pro-bounds-pointer-arithmetic,-cppcoreguidelines-pro-type-const-cast,-cppcoreguidelines-pro-type-cstyle-cast,-cppcoreguidelines-pro-type-reinterpret-cast,-cppcoreguidelines-pro-type-union-access,-cppcoreguidelines-pro-type-vararg,-cppcoreguidelines-special-member-functions,-fuchsia-*,-google-*,google-default-arguments,google-explicit-constructor,google-runtime-operator,-hicpp-avoid-goto,-hicpp-braces-around-statements,-hicpp-named-parameter,-hicpp-no-array-decay,-hicpp-no-assembler,-hicpp-no-malloc,-hicpp-function-size,-hicpp-special-member-functions,-hicpp-vararg,-llvm-*,-objc-*,-readability-else-after-return,-readability-implicit-bool-conversion,-readability-magic-numbers,-readability-named-parameter,-readability-simplify-boolean-expr,-readability-braces-around-statements,-readability-identifier-naming,-readability-function-size,-readability-redundant-member-init,-misc-bool-pointer-implicit-conversion,-misc-definitions-in-headers,-misc-unused-alias-decls,-misc-unused-parameters,-misc-unused-using-decls,-modernize-use-using,-modernize-use-default-member-init,-clang-diagnostic-*,-clang-analyzer-*,-zircon-*

int main(int  /*argc*/, char**  /*argv*/) {
    model::Character playerCharacter("Player");

    model::CharacterList characterList;
    characterList.addCharacter(std::move(std::make_shared<model::Character>("Enemy1", 10, 10, 100)));

    command::CommandFactory commandFactory(characterList);

    event::EventQueue::getInstance().addSubscriber(
        EVENT_TYPE_TICK,
        std::unique_ptr<event::EventSubscriber>(new event::TickEventSubscriber())
    );
    event::EventQueue::getInstance().addSubscriber(
        EVENT_TYPE_TURN,
        std::unique_ptr<event::EventSubscriber>(new event::TurnEventSubscriber())
    );
    event::EventQueue::getInstance().addSubscriber(
        EVENT_TYPE_CMD,
        std::unique_ptr<event::EventSubscriber>(new event::CommandEventSubscriber(playerCharacter, commandFactory))
    );

    engine::Engine::getInstance().start();

    return 0;
}
