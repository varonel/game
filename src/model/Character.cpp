#include "Character.h"

#include <iostream>

namespace model {
    void Character::move(int x, int y) {
        posX += x;
        posY += y;

        std::cout << name << " moved to " << posX << "x" << posY << std::endl;
    }

    Character::Character(std::string name) : name(std::move(name)) {}

    Character::Character(std::string name, const int posX, const int posY, const int hp)
            : name(std::move(name)), posX(posX), posY(posY), hp(hp) {}

    void Character::shoot(Character* character) const {
        character->loseHp(1);

        using std::cout;
        using std::endl;

        cout << name << " damaged " << character->getName() << " for 1" << endl;
        cout << character->getName() << " now has " << character->getHp() << endl;
    }

    const std::string& Character::getName() const {
        return name;
    }

    int Character::getHp() const {
        return hp;
    }

    void Character::loseHp(const int lost) {
        hp -= lost;
    }
}  // namespace model
