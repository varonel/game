#include "ExitCommand.h"

#include <iostream>
#include "../../engine/src/Engine.h"

namespace command {
    void ExitCommand::execute() {
        std::cout << "Bye bye!" << std::endl;
        engine::Engine::getInstance().stop = true;
    }
} // namespace command
