#ifndef GAME_CHARACTERLIST_H
#define GAME_CHARACTERLIST_H

#include <unordered_map>
#include <memory>
#include "Character.h"

namespace model {
    using CharacterMap = std::unordered_map<std::string, std::shared_ptr<Character>>;

    class CharacterList {
    public:
        void addCharacter(std::shared_ptr<Character> ch);

        CharacterMap::const_iterator find(const std::string& name) const;
        CharacterMap::const_iterator begin() const;
        CharacterMap::const_iterator end() const;
    private:
        CharacterMap characterMap;
    };
}  // namespace model

#endif //GAME_CHARACTERLIST_H
