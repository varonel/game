#ifndef GAME_SHOOTCOMMAND_H
#define GAME_SHOOTCOMMAND_H

#include <memory>
#include "../../engine/src/command/BaseCommand.h"
#include "../model/Character.h"

namespace command {
    class ShootCommand : public command::BaseCommand {
    public:
        ShootCommand(const model::Character& source, model::Character* target);

        void execute() override;

    private:
        const model::Character& source;
        model::Character* target;
    };
}  // namespace command

#endif //GAME_SHOOTCOMMAND_H
