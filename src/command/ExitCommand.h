#ifndef GAME_EXITCOMMAND_H
#define GAME_EXITCOMMAND_H

#include "../../engine/src/command/BaseCommand.h"

namespace command {
    class ExitCommand : public BaseCommand {
    public:
        void execute() override;
    };
} // namespace command

#endif //GAME_EXITCOMMAND_H
