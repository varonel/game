#include "ShootCommand.h"

#include <iostream>

namespace command {
    void ShootCommand::execute() {
        if (target == nullptr) {
            std::cout << "Cannot find target" << std::endl;

            return;
        }

        source.shoot(target);
    }

    ShootCommand::ShootCommand(const model::Character &source, model::Character* target)
        : source(source), target(target) {}
}  // namespace command
