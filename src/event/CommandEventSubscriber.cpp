#include <iostream>
#include "CommandEventSubscriber.h"
#include "../command/ExitCommand.h"
#include "../command/CommandFactory.h"

namespace event {
    void CommandEventSubscriber::run(const EventType &e) {
        std::unique_ptr<command::BaseCommand> cmd = commandFactory.makeCommand(e.cmd, e.context, character);

        if (cmd == nullptr) {
            std::cout << "Invalid command " << e.cmd << std::endl;
            return;
        }

        cmd->execute();
    }

    CommandEventSubscriber::CommandEventSubscriber(model::Character &character, command::CommandFactory &commandFactory)
        : character(character), commandFactory(commandFactory) {}
}  // namespace event
