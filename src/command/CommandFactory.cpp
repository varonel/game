#include "CommandFactory.h"

#include <iostream>
#include <map>

#include "../model/Character.h"
#include "ExitCommand.h"
#include "MoveCommand.h"
#include "ShootCommand.h"

namespace command {
    std::unique_ptr<BaseCommand> CommandFactory::makeCommand(
            const std::string& cmd,
            const std::vector<std::string>& context,
            model::Character& character
    ) {
        static const std::map<std::string, CommandFactory::commands> commandMap = {
                {"exit",  CommandFactory::EXIT},
                {"e",     CommandFactory::E},
                {"w",     CommandFactory::W},
                {"s",     CommandFactory::S},
                {"n",     CommandFactory::N},
                {"shoot", CommandFactory::SHOOT},
        };

        std::unique_ptr<BaseCommand> c;

        auto findCmd = commandMap.find(cmd);
        if (findCmd == commandMap.end()) {
            return c;
        }

        switch (findCmd->second) {
            case CommandFactory::EXIT:
                c = std::unique_ptr<BaseCommand>(new ExitCommand());
                break;
            case CommandFactory::E:
                c = std::unique_ptr<BaseCommand>(new MoveCommand(character, 1, 0));
                break;
            case CommandFactory::W:
                c = std::unique_ptr<BaseCommand>(new MoveCommand(character, -1, 0));
                break;
            case CommandFactory::S:
                c = std::unique_ptr<BaseCommand>(new MoveCommand(character, 0, 1));
                break;
            case CommandFactory::N:
                c = std::unique_ptr<BaseCommand>(new MoveCommand(character, 0, -1));
                break;
            case CommandFactory::SHOOT:
                model::Character* enemy = nullptr;

                if (!context.empty()) {
                    std::string enemyName = context[0];
                    auto iterator = characterList.find(enemyName);

                    if (iterator != characterList.end()) {
                        enemy = iterator->second.get();
                    }
                }

                c = std::unique_ptr<BaseCommand>(new ShootCommand(character, enemy));

                break;
        }

        return c;
    }

    CommandFactory::CommandFactory(model::CharacterList& characterList) : characterList(characterList) {}
}  // namespace command
