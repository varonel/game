#include <iostream>
#include "include/SDL2/SDL.h"

using namespace std;

#define WIN_WIDTH 1920
#define WIN_HEIGHT 1080


int main(int argc, char** argv) {
    if(SDL_Init(SDL_INIT_EVERYTHING) == -1){
        cout << "Something went wrong! " << SDL_GetError() << endl;
    }

    auto *window = SDL_CreateWindow("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_OPENGL);

    auto *screenSurface = SDL_GetWindowSurface( window );
    auto *image = SDL_LoadBMP( "assets/forest scenery.bmp" );

    auto *optimizedImage = SDL_ConvertSurface( image, screenSurface->format, 0 );
    SDL_FreeSurface( image );

    SDL_Rect stretchRect;
    stretchRect.x = 0;
    stretchRect.y = 0;
    stretchRect.w = WIN_WIDTH;
    stretchRect.h = WIN_HEIGHT;
    SDL_BlitScaled( optimizedImage, nullptr, screenSurface, &stretchRect);

    //    SDL_BlitSurface( optimizedImage, nullptr, screenSurface, nullptr );
    SDL_UpdateWindowSurface( window );

    bool done = false;

    while (!done) {
        SDL_Event e;
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                done = true;
            }
        }
    }

    SDL_FreeSurface( optimizedImage );
    SDL_DestroyWindow( window );
    SDL_Quit();

    return 0;
}
