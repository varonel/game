#ifndef ENGINE_EVENTSUBSCRIBER_H
#define ENGINE_EVENTSUBSCRIBER_H

#include "EventType.h"

namespace event {
    namespace _internal {
        class EventSubscriber {
        public:
            virtual void run(const EventType &e);
        };
    } // namespace _internal

    using _internal::EventSubscriber;
} // namespace event

#endif //ENGINE_EVENTSUBSCRIBER_H
