#include "CharacterList.h"

namespace model {
    void CharacterList::addCharacter(std::shared_ptr<Character> ch) {
        characterMap.insert(std::make_pair(ch->getName(), std::move(ch)));
    }

    CharacterMap::const_iterator CharacterList::begin() const {
        return characterMap.begin();
    }

    CharacterMap::const_iterator CharacterList::end() const {
        return characterMap.end();
    }

    CharacterMap::const_iterator CharacterList::find(const std::string& name) const {
        auto ch = characterMap.find(name);

        return ch;
    }
}  // namespace model
