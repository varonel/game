#ifndef ENGINE_EVENTQUEUE_H
#define ENGINE_EVENTQUEUE_H

#include <queue>
#include <string>
#include <map>
#include <mutex>
#include <condition_variable>
#include "EventSubscriber.h"
#include "EventType.h"

namespace event {
    namespace _internal {
        class EventQueue {
        public:
            static EventQueue& getInstance();

            void queueEvent(const EventType &event);
            bool hasEvents();
            void processNextEvent();
            void addSubscriber(int id, unique_ptr<EventSubscriber> subscriber);
            virtual ~EventQueue();

        private:
            EventQueue();
            EventQueue(EventQueue const&);
            void operator=(EventQueue const&);

            mutex writeMutex;
            condition_variable cond;
            queue<EventType> eventsQueue;
            map<int, vector<unique_ptr<EventSubscriber>>> subscribers;
        };
    }  // namespace _internal

    using _internal::EventQueue;
}  // namespace event

#endif //ENGINE_EVENTQUEUE_H
