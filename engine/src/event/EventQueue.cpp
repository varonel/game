#include "EventQueue.h"

namespace event {
    namespace _internal {

        EventQueue::EventQueue() = default;

        EventQueue& EventQueue::getInstance() {
            static EventQueue q;

            return q;
        }

        void EventQueue::queueEvent(const EventType& event) {
            unique_lock<mutex> mutexLock(writeMutex);
            eventsQueue.push(event);
            mutexLock.unlock();
            cond.notify_one();
        }

        bool EventQueue::hasEvents() {
            return !eventsQueue.empty();
        }

        void EventQueue::processNextEvent() {
            unique_lock<mutex> mutexLock(writeMutex);

            while (eventsQueue.empty()) {
                cond.wait(mutexLock);
            }

            const EventType& event = eventsQueue.front();

            auto subscriberIt = subscribers.find(event.id);
            if (subscriberIt != subscribers.end()) {
                for (auto& subscriber : subscriberIt->second) {
                    subscriber->run(event);
                }
            }

            eventsQueue.pop();
        }

        void EventQueue::addSubscriber(int id, unique_ptr<EventSubscriber> subscriber) {
            auto subscriberIt = subscribers.find(id);

            if (subscriberIt == subscribers.end()) {
                vector<unique_ptr<EventSubscriber>> newSubscribers;
                newSubscribers.push_back(move(subscriber));

                subscribers.insert(make_pair(id, move(newSubscribers)));

                return;
            }

            subscriberIt->second.push_back(move(subscriber));
        }

        EventQueue::~EventQueue() = default;
    }  // namespace _internal
}  // namespace event
