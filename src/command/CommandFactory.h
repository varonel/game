#ifndef GAME_COMMANDFACTORY_H
#define GAME_COMMANDFACTORY_H

#include "../../engine/src/command/BaseCommand.h"
#include "../model/Character.h"
#include "../model/CharacterList.h"
#include <vector>

namespace command {
    class CommandFactory {
    public:
        std::unique_ptr<BaseCommand> makeCommand(const std::string &cmd, const std::vector<std::string>& context, model::Character &character);

        explicit CommandFactory(model::CharacterList &characterList);

    private:
        enum commands {
            EXIT, E, W, S, N, SHOOT
        };
        model::CharacterList &characterList;
    };
}  // namespace command

#endif //GAME_COMMANDFACTORY_H
