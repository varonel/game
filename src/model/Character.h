#ifndef GAME_CHARACTER_H
#define GAME_CHARACTER_H

#include <string>

namespace model {
    class Character {
    public:
        Character(std::string name, int posX, int posY, int hp = 100);

        explicit Character(std::string name);

        Character(const Character& ch) = delete;

        Character& operator=(const Character& ch) = delete;

        Character(Character&& ch) noexcept = default;

        Character& operator=(Character&& ch) noexcept = default;

        ~Character() = default;

        const std::string& getName() const;

        int getHp() const;

        void loseHp(int lost);

        void move(int x, int y);

        void shoot(Character* character) const;

    private:
        std::string name;
        int posX = 0;
        int posY = 0;
        int hp = 100;
    };
}  // namespace model

#endif //GAME_CHARACTER_H
