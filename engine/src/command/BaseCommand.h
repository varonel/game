#ifndef GAME_BASECOMMAND_H
#define GAME_BASECOMMAND_H

namespace command {
    class BaseCommand {
    public:
        virtual void execute() = 0;
    };
}

#endif //GAME_BASECOMMAND_H
