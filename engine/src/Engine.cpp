#include <thread>
#include <iostream>
#include <string>
#include <sstream>
#include "Engine.h"

namespace engine {
        Engine::Engine() = default;

        Engine& Engine::getInstance() {
            static Engine e;

            return e;
        }

        void Engine::start() {
            std::thread mainLoopThread(&Engine::mainloop, this);
            std::thread eventProcessorThread(&Engine::processEventsLoop, this);
            std::thread cmdProcessorThread(&Engine::readInput, this);

            mainLoopThread.join();
            eventProcessorThread.join();
            cmdProcessorThread.join();
        }

        void Engine::mainloop() {
            while (!stop) {
                std::this_thread::sleep_for(std::chrono::seconds(1));

                auto tick = event::EventType(EVENT_TYPE_TICK);
                event::EventQueue::getInstance().queueEvent(tick);
            }
        }

        void Engine::processEventsLoop() {
            while (!stop) {
                event::EventQueue::getInstance().processNextEvent();
            }
        }

        void Engine::readInput() {
            do {
                using namespace std;

                string input;
                getline(cin, input);

                vector<string> cmdArgs;
                stringstream cmdStream(input);
                string buffer;
                string cmd;

                getline(cmdStream, cmd, ' ');

                while (getline(cmdStream, buffer, ' '))
                {
                    cmdArgs.push_back(move(buffer));
                }

                auto commandEvent = event::EventType(EVENT_TYPE_CMD, move(cmd), move(cmdArgs));
                event::EventQueue::getInstance().queueEvent(commandEvent);

                // Wait for events to be processed before reading input
                while (event::EventQueue::getInstance().hasEvents()) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                }
            } while (!stop);
        }

        Engine::~Engine() = default;
}  // namespace engine
