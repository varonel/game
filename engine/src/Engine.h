#ifndef ENGINE_ENGINE_H
#define ENGINE_ENGINE_H

#include "event/EventQueue.h"

namespace engine {
    class Engine {
    public:
        static Engine &getInstance();

        void start();

        bool stop = false;

    private:
        Engine();

        Engine(Engine const &);

        Engine(Engine const &&) noexcept;

        void operator=(Engine const &);

        void operator=(Engine const &&) noexcept;

        void mainloop();

        void processEventsLoop();

        void readInput();

    public:
        virtual ~Engine();
    };
}  // namespace engine

#endif //ENGINE_ENGINE_H
