#ifndef ENGINE_COMMANDEVENTSUBSCRIBER_H
#define ENGINE_COMMANDEVENTSUBSCRIBER_H

#include "../../engine/src/event/EventSubscriber.h"
#include "../model/Character.h"
#include "../command/CommandFactory.h"

namespace event {
    class CommandEventSubscriber : public EventSubscriber {
    public:
        void run(const EventType &e) override;

        CommandEventSubscriber(model::Character &character, command::CommandFactory &commandFactory);

    private:
        model::Character& character;
        command::CommandFactory &commandFactory;
    };
}  // namespace event

#endif //ENGINE_COMMANDEVENTSUBSCRIBER_H
