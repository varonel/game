#include <process.h>
#include <iostream>
#include "TickEventSubscriber.h"

namespace event {
    void TickEventSubscriber::run(const EventType &e) {}
} // namespace event
