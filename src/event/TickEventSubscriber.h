#ifndef ENGINE_TICKEVENTSUBSCRIBER_H
#define ENGINE_TICKEVENTSUBSCRIBER_H

#include "../../engine/src/event/EventSubscriber.h"

namespace event {
    class TickEventSubscriber : public EventSubscriber {
    public:
        void run(const EventType &e) override;
    };
} // namespace event

#endif //ENGINE_TICKEVENTSUBSCRIBER_H
